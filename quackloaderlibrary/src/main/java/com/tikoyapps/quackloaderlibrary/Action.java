package com.tikoyapps.quackloaderlibrary;

import java.lang.ref.WeakReference;

/**
 * Created by xcptan on 31/08/2016.
 */
public class Action<T> {
    String key;
    WeakReference<T> target;

    public Action(String key, WeakReference<T> target) {
        this.key = key;
        this.target = target;
    }

    public String getKey() {
        return key;
    }

    public WeakReference<T> getTarget() {
        return target;
    }
}
