package com.tikoyapps.quackloaderlibrary;

import android.app.ActivityManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.util.LruCache;
import android.view.Display;
import android.view.WindowManager;
import android.widget.ImageView;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Created by xcptan on 30/08/2016.
 */
public class QuackLoader {

    private static final int LOAD_IMAGE_SUCCESS = 200;
    private static final int LOAD_JSON_SUCCESS = 201;

    private Executor mExecutor;

    private static QuackLoader mInstance;

    private static final int KEEP_ALIVE_TIME = 1;

    private ThreadFactory mThreadFactory;

    private LinkedBlockingQueue<Runnable> mWorkQueue;

    private Handler mHandler;

    private LruCache<String, Bitmap> mCache;

    private int maxWidth;
    private int maxHeight;

    //Defensive call to getApplicationContext to prevent memory leaks
    private Context mContext;

    private int getCacheMaxSize(Context context) {
        int memClass =
            ((ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE)).getMemoryClass();
        int cacheSize = 1024 * 1024 * memClass / 8;
        Log.d("QuackLoader", String.valueOf(cacheSize));
        return cacheSize;
    }

    private QuackLoader(Context context) {

        this.mContext = context.getApplicationContext();

        WindowManager wm = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point p = new Point();
        display.getSize(p);
        maxWidth = p.x;
        maxHeight = p.y;

        mCache = new LruCache<String, Bitmap>(getCacheMaxSize(mContext)) {
            @Override
            protected int sizeOf(String key, Bitmap bitmap) {
                return bitmap.getByteCount() / 1024;
            }
        };

        mThreadFactory = new ThreadFactory() {
            @Override
            public Thread newThread(Runnable r) {
                return new Thread(r);
            }
        };

        mWorkQueue = new LinkedBlockingQueue<Runnable>();

        mHandler = new Handler(Looper.getMainLooper()) {
            @Override
            public void handleMessage(final Message msg) {
                switch (msg.what) {
                    case LOAD_IMAGE_SUCCESS:
                        post(new Runnable() {
                            @Override
                            public void run() {
                                Action<ImageView> action = (Action<ImageView>) msg.obj;
                                action.getTarget().get().setImageBitmap(mCache.get(action.key));
                            }
                        });
                        break;
                }
            }
        };

        mExecutor =
            new ThreadPoolExecutor(1, Runtime.getRuntime().availableProcessors(), KEEP_ALIVE_TIME,
                TimeUnit.SECONDS, mWorkQueue, mThreadFactory);
    }

    public static QuackLoader with(Context context) {
        if (null == mInstance) {
            synchronized (QuackLoader.class) {
                if (null == mInstance) {
                    mInstance = new QuackLoader(context);
                }
            }
        }
        return mInstance;
    }

    public void loadImage(final String urlString, final ImageView imageView) {
        mExecutor.execute(new Runnable() {
            @Override
            public void run() {
                try {

                    Bitmap b = mCache.get(urlString);
                    if (b == null) {
                        mCache.put(urlString, decodeFromStream(urlString));
                    }

                    Message message = new Message();
                    message.obj =
                        new Action<ImageView>(urlString, new WeakReference<ImageView>(imageView));
                    message.what = LOAD_IMAGE_SUCCESS;
                    mHandler.dispatchMessage(message);
                }
                catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private Bitmap decodeFromStream(String urlString) throws IOException {

        URL url = new URL(urlString);
        HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();

        InputStream inputStream = new BufferedInputStream(httpURLConnection.getInputStream());

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(inputStream, null, options);
        inputStream.close();
        httpURLConnection.disconnect();

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, maxWidth, maxHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;

        httpURLConnection = (HttpURLConnection) url.openConnection();
        inputStream = new BufferedInputStream(httpURLConnection.getInputStream());
        Bitmap bitmap = BitmapFactory.decodeStream(inputStream, null, options);
        inputStream.close();
        httpURLConnection.disconnect();

        return bitmap;
    }

    private String read(InputStream is) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        StringBuilder response = new StringBuilder();
        String data;
        while ((data = br.readLine()) != null) {
            response.append(data);
        }
        return response.toString();
    }

    private int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }
}
