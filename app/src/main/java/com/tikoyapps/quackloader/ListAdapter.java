package com.tikoyapps.quackloader;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.tikoyapps.quackloaderlibrary.QuackLoader;
import java.util.List;

/**
 * Created by xcptan on 31/08/2016.
 */
public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ListVH> {

    private List<String> mList;
    private Context mContext;

    public ListAdapter(Context context, List<String> list) {
        this.mList = list;
        this.mContext = context;
    }

    @Override
    public ListVH onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.list_item, parent, false);
        return new ListVH(v);
    }

    @Override
    public void onBindViewHolder(ListVH holder, int position) {
        String imageUrl = mList.get(position);
        QuackLoader.with(mContext).loadImage(imageUrl, holder.imageView);
        holder.itemView.setTag(imageUrl);
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public static class ListVH extends RecyclerView.ViewHolder {
        ImageView imageView;

        public ListVH(View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.list_item_imageview);
        }
    }
}
