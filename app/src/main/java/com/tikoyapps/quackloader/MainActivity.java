package com.tikoyapps.quackloader;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by xcptan on 31/08/2016.
 */
public class MainActivity extends AppCompatActivity {

    RecyclerView mRecyclerView;
    ListAdapter mListAdapter;

    @Override
    protected void onCreate(
        @Nullable
        Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        List<String> imageUrls = new ArrayList<String>(){{
            this.add("http://placecorgi.com/260/180");
            this.add("http://placecorgi.com/270/190");
            this.add("http://placecorgi.com/280/200");
            this.add("http://placecorgi.com/360/280");
            this.add("http://placecorgi.com/370/290");
            this.add("http://placecorgi.com/380/300");
            this.add("http://placecorgi.com/360/310");
            this.add("http://placecorgi.com/370/320");
            this.add("http://placecorgi.com/380/330");

            this.add("http://placecorgi.com/300/300");
            this.add("http://placecorgi.com/302/301");
            this.add("http://placecorgi.com/303/303");
            this.add("http://placecorgi.com/304/304");
            this.add("http://placecorgi.com/305/305");
            this.add("http://placecorgi.com/306/306");
            this.add("http://placecorgi.com/307/307");
            this.add("http://placecorgi.com/308/308");
            this.add("http://placecorgi.com/309/309");

            this.add("http://placecorgi.com/400/400");
            this.add("http://placecorgi.com/402/401");
            this.add("http://placecorgi.com/403/403");
            this.add("http://placecorgi.com/404/404");
            this.add("http://placecorgi.com/405/405");
            this.add("http://placecorgi.com/406/406");
            this.add("http://placecorgi.com/407/407");
            this.add("http://placecorgi.com/408/408");
            this.add("http://placecorgi.com/409/409");

            this.add("http://placecorgi.com/500/500");
            this.add("http://placecorgi.com/502/501");
            this.add("http://placecorgi.com/505/505");
            this.add("http://placecorgi.com/504/504");
            this.add("http://placecorgi.com/505/505");
            this.add("http://placecorgi.com/506/506");
            this.add("http://placecorgi.com/507/507");
            this.add("http://placecorgi.com/508/508");
            this.add("http://placecorgi.com/509/509");
        }};




        mRecyclerView = (RecyclerView) findViewById(R.id.listview);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));
        mListAdapter = new ListAdapter(this,imageUrls);
        mRecyclerView.setAdapter(mListAdapter);
    }
}
